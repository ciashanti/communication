import { Component, Injectable, OnInit } from '@angular/core';
import { ApiService } from "../service/api.service";
import { UserService } from "../service/user.service";
import { ActivatedRoute, Router, ParamMap,CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Route } from "@angular/router";
import {
  HttpClient,
  HttpRequest,
  HttpResponse,
  HttpParams,
  HttpHeaders
  
} from "@angular/common/http";

//-----------------------------------------------------------snackbar
 import {MatSnackBar} from '@angular/material/snack-bar';
//-----------------------------------------------------------snackbar

@Injectable()
@Component({
  selector: 'app-loginx',
  templateUrl: './loginx.component.html',
  styleUrls: ['./loginx.component.css']
})
export class LoginxComponent implements OnInit {
 hide = true;
  id: string = "";
  username: any;
  userName: any;
  password: any;
  passWord: any;
  mypost: any;
  mypostt: any;
  error: string = "";
  mycont: any;
  myurl: any;
  base_urlarr: any;
  url: any;
  emp_id: any;
  role: any;
  isLoggedIn:boolean=true;
  constructor(
    private http: HttpClient,
    private apiservice: ApiService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
   
    //-----------------------------------------------------------snackbar
      public SnackBar: MatSnackBar 
    //-----------------------------------------------------------snackbar
  ) {
    
    this.url =
      "http://ec2-18-218-68-83.us-east-2.compute.amazonaws.com/flaskapp/user_tracking/UserTracking/verifyUsername";
  }
  ngOnInit() {
    
  }
  
  // -------------------------------------------------------------login
 
  getLogin() {
    console.log(this.url);
    console.log(this.username);
    console.log(this.password);
    this.http
      .get(this.url + "/" + this.username + "/" + this.password)
      .subscribe(res => {
        this.mypost = res;
        this.mypostt = this.mypost.responseList;
        this.emp_id = this.mypostt.emp_id;
        this.role = this.mypostt.emp_role;
        console.log(this.mypost);
        console.log(this.emp_id);
        console.log(this.role);
        
        if (
          this.mypost.attributes.status == "success" &&
          this.mypost.attributes.message == "Login Success"
        ) {
          this.router.navigate(["Dashboard"]);
          this.isLoggedIn = true;
          this.SnackBar.open("Login successfully", "OK", {
            data: " ",
            duration: 3000
          });
         
        } else {
          this.SnackBar.open("Failed", "RETRY", {
            data: " ",
            duration: 3000
          });
          this.router.navigate(["/login"]);
        }
        return this.mypostt;
      });
  }
  
  // -------------------------------------------------------------login

  
}