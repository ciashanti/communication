import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpEvent,
  HttpErrorResponse,
  HttpEventType
} from "@angular/common/http";

import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
import { map } from "rxjs/operators";
import { ApiService } from "../service/api.service";
//import { RequestOptions, Headers, Http } from "@angular/common/http";

@Injectable()
export class UserService {
  public url;
  public onlineurl;
  public localurl;
  constructor(private http: HttpClient, private apiservice: ApiService) {
    this.url = localStorage.getItem("url");
    this.localurl = "http://localhost:8080";
    this.onlineurl = "http://creamsonservices.com:8080";
  }


  //--------------------------------------------------admin_portal-------------------------------------------

  getlogin() {
    let api_url =
      "http://ec2-18-218-68-83.us-east-2.compute.amazonaws.com/flaskapp/user_tracking/UserTracking/verifyUsername/";
    return this.apiservice.get(api_url);
  }
  
  getboard() {
    let api_url =
      " http://ec2-18-218-68-83.us-east-2.compute.amazonaws.com/flaskapp/board";
    return this.apiservice.get(api_url);
  }
  getactivity() {
    let api_url =
      "http://ec2-18-218-68-83.us-east-2.compute.amazonaws.com/flaskapp/activity";
    return this.apiservice.get(api_url);
  }
  getfiletype() {
    let api_url =
      "http://ec2-18-218-68-83.us-east-2.compute.amazonaws.com/flaskapp/filetype";
    return this.apiservice.get(api_url);
  }
  getcontent() {
    let api_url =
      "http://ec2-18-218-68-83.us-east-2.compute.amazonaws.com/flaskapp/content";
    return this.apiservice.get(api_url);
  }
  getassesment() {
    let api_url =
      "http://ec2-18-218-68-83.us-east-2.compute.amazonaws.com/flaskapp/assessment_type";
    return this.apiservice.get(api_url);
  }
  getallassesment() {
    let api_url =
      "http://ec2-18-http://ec2-18-218-68-83.us-east-2.compute.amazonaws.com/flaskapp/get_assessment";
    return this.apiservice.get(api_url);
  }

  public upload(data) {
    return this.http
      .post<any>(
        "http://creamsonservices.com:8080/UploadImagesAndVideos/VideoUploader",
        data,
        {
          reportProgress: true,
          observe: "events"
        }
      )
      .pipe(
        map(event => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              const progress = Math.round((100 * event.loaded) / event.total);
              return { status: "progress", message: progress };

            case HttpEventType.Response:
              return event.body;
            default:
              return `Unhandled event: ${event.type}`;
          }
        })
      );
  }

  public uploadi(data) {
    return this.http
      .post<any>(
        "http://creamsonservices.com:8080/UploadImagesAndVideos/ImageUploader",
        data,
        {
          reportProgress: true,
          observe: "events"
        }
      )
      .pipe(
        map(event => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              const progress = Math.round((100 * event.loaded) / event.total);
              return { status: "progress", message: progress };

            case HttpEventType.Response:
              return event.body;
            default:
              return `Unhandled event: ${event.type}`;
          }
        })
      );
  }

  // public uploadt(data) {
  //   return this.http
  //     .post<any>(
  //       "http://creamsonservices.com:8080/UploadImagesAndVideos/textOrPdfUpload?fileType=TEXT",
  //       data,
  //       {
  //         reportProgress: true,
  //         observe: "events"
  //       }
  //     )
  //     .pipe(
  //       map(event => {
  //         switch (event.type) {
  //           case HttpEventType.UploadProgress:
  //             const progress = Math.round((100 * event.loaded) / event.total);
  //             return { status: "progress", message: progress };

  //           case HttpEventType.Response:
  //             return event.body;
  //           default:
  //             return `Unhandled event: ${event.type}`;
  //         }
  //       })
  //     );
  // }
}
//http://192.168.31.196:8090/CreamsonIntelliLanguagelabservices-0.0.2-SNAPSHOT/lab-activities/ ............. dada
// http://192.168.31.19:8080/lab-activities/
