import { Component } from '@angular/core';
import {  Compiler } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'communication';
  constructor(private _compiler: Compiler) {
    this._compiler.clearCache();
}
}
